package pl.krze.model;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * Created by Krze on 2016-11-21.
 */

@Component
public class GbrCount implements CurrencyInterface {

    static final double gbrFixedCosts = 600d;
    static double exchangeRate;

    @Override
    public double getExchangeRate() {
        exchangeRate = 4.80;
      return exchangeRate;
    }

    public CountryJsonData fetchGbrCurrency() {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject("http://api.nbp.pl/api/exchangerates/rates/a/gbp/?format=json", CountryJsonData.class);
    }


}
