package pl.krze.model;

/**
 * Created by Krze on 2016-11-21.
 */
public interface CurrencyInterface {
    double getExchangeRate();

}
