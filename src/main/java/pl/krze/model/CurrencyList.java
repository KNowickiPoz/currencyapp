package pl.krze.model;

/**
 * Created by Krze on 2016-11-22.
 */
public enum CurrencyList {
    GBR("British pound"),
    EUR("Euro"),
    PLN("Polish zloty");

    private String description;

    CurrencyList(final String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }
}
