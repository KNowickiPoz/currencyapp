package pl.krze.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.stereotype.Component;

/**
 * Created by Krze on 2016-11-21.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@Component
public class Rates {

    private Double mid;
    private Double salary;


    public Double getMid() {
        return mid;
    }

    public void setMid(Double mid) {
        this.mid = mid;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public Rates() {

    }




}
