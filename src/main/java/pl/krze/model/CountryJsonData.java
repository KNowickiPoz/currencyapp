package pl.krze.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Krze on 2016-11-21.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@Component
public class CountryJsonData {

    @Autowired
    private Rates rates;

//    private String code;
    public Double newSalary;

//    public String getCode() {
//        return code;
//    }
//
//    public void setCode(String code) {
//        this.code = code;
//    }

    public Rates getRates() {
        return rates;
    }

    public void setRates(Rates rates) {
        this.rates = rates;
    }

    public CountryJsonData() {
    }

    public double calculateEarnings() {
        newSalary = rates.getSalary() * 22;
        return newSalary;
    }
}
