package pl.krze.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.krze.model.CountryJsonData;
import pl.krze.model.Rates;

/**
 * Created by Krze on 2016-11-20.
 */

@Controller
public class HomeController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Rates rates) {
        return "home";
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String calculate(Rates rates, CountryJsonData countryJsonData, Model model) {
        model.addAttribute("salary", rates.getSalary());
        model.addAttribute("newSalary", countryJsonData.calculateEarnings());
        return "result";
    }

}
